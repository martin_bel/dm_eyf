vars_nuevas <- function(d){
  d[,num_total_dias_mora:=(fecha_Master_Finiciomora + fecha_Visa_Finiciomora)]
  d[,fecha_dif_fvenc_mora:=(fecha_Visa_Fvencimiento + fecha_Visa_Finiciomora)]
  
  # ok
  d[,fl_dias_mora:=ifelse(num_total_dias_mora > 0, 1, 0)]
  d[,fl_tj_paquetes:=(fl_tpaquete2+fl_tpaquete3+ fl_tpaquete4+fl_tpaquete5+
                        fl_tpaquete6+fl_tpaquete7+fl_tpaquete8+fl_tpaquete9)]
  
  d[,nom_tj_estado_cuenta_vm:=ifelse(nom_Master_cuenta_estado == nom_Visa_cuenta_estado, 1, 0)]
  d[,fl_tj_tconsumo:=(fl_Master_tconsumos + fl_Visa_tconsumos)]
  d[,fl_tj_marca_atraso:=(fl_Master_marca_atraso + fl_Visa_marca_atraso)]
  d[,fl_tj_tadelantosefectivo:=(fl_Master_tadelantosefectivo + fl_Visa_tadelantosefectivo)]
  d[,num_rt_rent:=num_mrentabilidad/num_mrentabilidad_annual]
  
  d[,num_int_venc_mora_visa:=(fecha_Visa_Fvencimiento*fecha_Visa_Finiciomora)]
  d[,num_int_venc_mora_master:=(fecha_Master_Fvencimiento*fecha_Master_Finiciomora)]
  
  # Tarjetas
  
  d[,num_tj_mfinanciacion_limite:=(num_Visa_mfinanciacion_limite+num_Master_mfinanciacion_limite)]
  d[,num_tj_msaldototal:=(num_Visa_msaldototal+num_Master_msaldototal)]
  
  d[,num_tj_msaldopesos:=(num_Visa_msaldopesos+num_Master_msaldopesos)]
  d[,num_tj_msaldodolares:=(num_Visa_msaldodolares+num_Master_msaldodolares)]
  d[,num_tj_mconsumospesos:=(num_Visa_mconsumospesos+num_Master_mconsumospesos)]
  d[,num_tj_mlimitecompra:=(num_Visa_mlimitecompra+num_Master_mlimitecompra)]
  d[,num_tj_madelantopesos:=(num_Visa_madelantopesos+num_Master_madelantopesos)]
  d[,num_tj_madelantodolares:=(num_Visa_madelantodolares+num_Master_madelantodolares)]
  d[,num_tj_madelantos_pd:=(num_tj_madelantopesos+num_tj_madelantodolares)]
  d[,num_tj_mpagado:=(num_Visa_mpagado+num_Master_mpagado)]
  d[,num_tj_mpagospesos:=(num_Visa_mpagospesos+num_Master_mpagospesos)]
  
  d[,num_tj_mpagosdolares:=(num_Visa_mpagosdolares+num_Master_mpagosdolares)]
  d[,num_tj_mconsumototal:=(num_Visa_mconsumototal+num_Master_mconsumototal)]
  d[,num_tj_mpagado:=(num_Visa_mpagado+num_Master_mpagado)]
  d[,num_tj_mpagominimo:=(num_Visa_mpagominimo+num_Master_mpagominimo)]
  
  d[,fl_tj_mpagado:=(num_Visa_mpagado+num_Master_mpagado)]
  d[,num_tj_mpagominimo:=(num_Visa_mpagominimo+num_Master_mpagominimo)]
  
  ### Limite de compra tarjeta
  d[,num_tj_consumo_limite:=(num_tj_mconsumototal/num_tj_mlimitecompra)]
  d[,num_visa_consumo_limite:=(num_Visa_mconsumototal/num_Visa_mlimitecompra)]
  d[,num_master_consumo_limite:=(num_Master_mconsumototal/num_Master_mlimitecompra)]
  
  d[,fl_tj_consumo_limite:=ifelse(is.na(num_tj_consumo_limite), -1,
                                  ifelse(num_tj_consumo_limite > 1, 1, 0))]
  return(d)
}

# d <- vars_nuevas(d)
# d[[1]] <- vars_nuevas(d[[1]])