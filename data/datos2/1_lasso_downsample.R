### Downsampling - modelos

rm(list = ls(all=TRUE))
library(data.table)
setwd('/Users/mbtangotan/Desktop/dm/dm_eyf/data/datos2')
source('preproc1.R')
source('/Users/mbtangotan/Desktop/dm/dm_eyf/data/datos/preproc_lasso.R')
source('/Users/mbtangotan/Desktop/dm/dm_eyf/data/datos/lasso_fun.R')

load('data_agg_mes_201403_down.RData')

library(data.table)
library(caret)

d <- preproc1(d)


print('Calculando matriz de correlacion')

tipo_dato <- td(train)
corrMatrix <- cor(train[, tipo_dato[['num']] ])
corr_vars <- findCorrelation(corrMatrix, cutoff = .9, verbose = F)
train <- train[, -corr_vars]
vars_train <- names(train)

not_in <- names(train)[!names(train) %in% names(test)]
vars_train <- vars_train[!vars_train %in% not_in]

train <- train[, vars_train]
test <- test[, vars_train]

d <- rbind(train, test)

na = apply(d, 2, function(x) sum(is.na(x)))
drop_na <- names(na[na != 0])

d <- d[, names(d)[!names(d) %in% drop_na]]
x_all = model.matrix(~ . -1, data = d[, names(d)[!names(d) %in% c('clase')]])

inx_train <- 1:nrow(train)
inx_test <- (nrow(train) + 1):(nrow(d))

x_train = x_all[inx_train,]
y_train = d[inx_train, 'clase']
x_test <- x_all[inx_test, ]
y_test = d[inx_test, 'clase']