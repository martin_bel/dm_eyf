setwd("/Users/mbtangotan/Desktop/dm/dm_eyf/python2")

### Obtengo all
source('create_agg_2.R')

options(scipen=666)
rm(list = ls(all=TRUE))

pkg = c('foreign', 'nnet', 'ggplot2', 'caret', 'glmnet', 
	    'doParallel', 'doMC', 'svmpath', 'LiblineaR',
	    'data.table', 'caret', 'rpart', 'pROC', 'doMC', 
	    'Hmisc')

sapply(pkg, require, c=T)

### Tomo como input all y genero distintos datasets

gen_datasets <- function(d=d, seed, p=0.5){
	set.seed(seed)
	inTrainingSet <- createDataPartition(
	  d$clase,
	  p = p,
	  list = FALSE
	)

	train <- d[inTrainingSet, ]
	ln <- apply(train, 2, function(x) length(unique(x)))

	d <- d[, names(d)[!names(d) %in% names(ln[ln == 1])]]
	train <- d[inTrainingSet, ]

	output = paste0("../data/data_agg_201404_seed", seed,".txt")
	write.table(train, output, sep="\t", quote=FALSE, 
		row.names=FALSE, col.names=TRUE, na = "")
}

d <- as.data.frame(all)
lapply(1:5, function(x){
	gen_datasets(d=d, x, p=0.25)
})

lapply(10:12, function(x){
	gen_datasets(d=d, x, p=0.5)
})



lapply(10:15, function(x){
	gen_datasets(d, x, p=0.1)
})



