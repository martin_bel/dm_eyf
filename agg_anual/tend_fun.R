tend_10 <- function(x){
  rt <- (x[1] - x[2])/mean(x)
  ifelse(is.na(rt), 999, ifelse(rt > 0.1, 1, ifelse(rt < 0.1, -1, 0)))
}

tend_25 <- function(x){
  rt <- (x[1] - x[2])/mean(x)
  ifelse(is.na(rt), 999, ifelse(rt > 0.25, 1, ifelse(rt < 0.25, -1, 0)))
}

tend_50 <- function(x){
  rt <- (x[1] - x[2])/mean(x)
  ifelse(is.na(rt), 999, ifelse(rt > 0.5, 1, ifelse(rt < 0.5, -1, 0)))
}

# trimestre 1 contra 3 y 4
tend_10b <- function(x){
  rt <- (x[1] - x[3])/mean(x[1], x[3])
  ifelse(is.na(rt), 999, ifelse(rt > 0.1, 1, ifelse(rt < 0.1, -1, 0)))
}

tend_25b <- function(x){
  rt <- (x[1] - x[3])/mean(x[1], x[3])
  ifelse(is.na(rt), 999, ifelse(rt > 0.25, 1, ifelse(rt < 0.25, -1, 0)))
}

tend_50b <- function(x){
  rt <- (x[1] - x[3])/mean(x[1], x[3])
  ifelse(is.na(rt), 999, ifelse(rt > 0.5, 1, ifelse(rt < 0.5, -1, 0)))
}


# tend <- function(x) {
#   t = (mean(c(x[1],x[2],x[3])) - mean(c(x[4],x[5],x[6]))) / mean(x)
#   if ( is.na(t) ) {
#     return(0)
#   } else {
#     return(ifelse(abs(t)<0.1,0,ifelse(t<0,-1,1)))
#   }
# }