agregar <- function(fechaMaxima, ld){
  DT <- ld$d
  max_mes <- ld$max_mes
  
  fl <- grep("fl_", names(DT), v=T)
  setkey(DT, id_numero_de_cliente)
  by_cols='id_numero_de_cliente'
  
  # Fechas
  nm = names(DT)
  vars = c(nm[!nm %in% c('clase', 'id_mes')])
  vars_fecha = grep('fecha', names(d), value=TRUE)
  fechaMaxima = as.numeric(as.POSIXct(as.character(fechaMaxima), format = "%Y%m%d"))
  for ( j in vars_fecha ) {
    set(DT, i=NULL, j=j, value=(fechaMaxima - as.numeric(as.POSIXct(as.character(DT[[j]]), format = "%Y%m%d")))/(24*60*60))
  }
  
  for(j in vars) set(DT, i=NULL, j=j, value=as.double(DT[[j]]))
  DT <- vars_nuevas(DT)
  for(j in vars) set(DT, i=NULL, j=j, value=as.double(DT[[j]]))
  
  print("Calculando variables aggregadas")
  print(Sys.time())
  clase <- DT[id_mes == 1, c('id_numero_de_cliente', 'clase'), with=F]
  min <- DT[, lapply(.SD, function(x) min(x, na.rm=T)), by=by_cols, .SDcols=vars]
  max <- DT[, lapply(.SD, function(x) max(x, na.rm=T)), by=by_cols, .SDcols=vars]
  mean <- DT[, lapply(.SD, function(x) mean(x, na.rm=T)), by=by_cols, .SDcols=vars]
  med <- DT[, lapply(.SD, function(x) median(x, na.rm=T)), by=by_cols, .SDcols=vars]
  count <- DT[, lapply(.SD, function(x) sum(is.na(x))), by=by_cols, .SDcols=vars]
  sum <- DT[, lapply(.SD, function(x) sum(x, na.rm=T)), by=by_cols, .SDcols=vars]
  n1 <- DT[id_mes == 1, names(DT)[!names(DT) %in% c('id_mes', 'clase')], with=F]
  na1 <- n1[, lapply(.SD, function(x) ifelse(is.na(x), 1, 0)), by=by_cols, .SDcols=vars] 
  
  prepare <- function(x, prefix) {
    names = paste0(prefix, names(x)[-1])
    names = c("id_numero_de_cliente", names)
    setnames(x, names)
    setkey(x, id_numero_de_cliente)
    return(x)
  }
  
  min = prepare(min, "min_")
  max = prepare(max, "max_")
  mean = prepare(mean, "mean_")
  med = prepare(med, "med_")
  count = prepare(count, "count_")
  sum = prepare(sum, "sum_")
  n1 = prepare(n1, "n1_")
  na1 = prepare(na1, "na1_")
  
  print('Aggregaciones basicas')
  agg1 <- n1[na1][min][max][mean][med][count][sum]
  
  print('Medias 12 13 123')
  mean_12 <- DT[id_mes %in% 1:2, lapply(.SD, function(x) mean(x, na.rm=T)), by=c('id_mes', by_cols), .SDcols=vars]
  mean_13 <- DT[id_mes %in% c(1:3), lapply(.SD, function(x) mean(x, na.rm=T)), by=c('id_mes', by_cols), .SDcols=vars]
  DT123 <- DT
  DT123[,id_mes:=ifelse(id_mes == 1, 1, 0)]
  mean_123 <- DT123[, lapply(.SD, function(x) mean(x, na.rm=T)), by=c('id_mes', by_cols), .SDcols=vars]
  
  print('Diferencias')
  diff12 <- mean_12[, lapply(.SD, function(x) x[1] - x[2]), by=by_cols, .SDcols=vars]
  diff13 <- mean_13[, lapply(.SD, function(x) x[1] - x[2]), by=by_cols, .SDcols=vars]
  diff123 <- mean_123[, lapply(.SD, function(x) x[1] - x[2]), by=by_cols, .SDcols=vars]
  prepare(diff12, "diff12_")
  prepare(diff13, "diff13_")
  prepare(diff123, "diff123_")
  
  print('Variaciones')
  var12 <- mean_12[, lapply(.SD, function(x) ( (x[1] - x[2])/ x[2] ) ), by=by_cols, .SDcols=vars]
  var13 <- mean_13[, lapply(.SD, function(x) ( (x[1] - x[2])/ x[2] ) ), by=by_cols, .SDcols=vars]
  var123 <- mean_123[, lapply(.SD, function(x) ( (x[1] - x[2])/ x[2] ) ), by=by_cols, .SDcols=vars] 
  prepare(var12, "var12_")
  prepare(var13, "var13_")
  prepare(var123, "var123_")
  
  print('Deja')
  dj12 <- mean_12[, lapply(.SD, function(x) as.double(x[1] < x[2]) ), by=by_cols, .SDcols=vars]
  dj123 <- mean_123[, lapply(.SD, function(x) as.double(x[1] < x[2]) ), by=by_cols, .SDcols=vars]
  prepare(dj12, "dj12_")
  prepare(dj123, "dj123_")
  
  print('Join Final')
  d <- agg1[diff12][diff13][diff123][var12][var13][var123][dj12][dj123][clase]
  drop <- grep('id_numero_de_cliente', names(d), value=T)[-1]
  d <- d[ , names(d)[!names(d) %in% drop], with=F]
  save(d, file=sprintf('agg_%s.RData', max_mes))
  d
}