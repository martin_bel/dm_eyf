﻿COMPUTE altura=10. 

COMPUTE alphasplit = 0.05. 
* default 0.05 spliting nodes 

COMPUTE alphamerge = 0.05.
* default 0.05 merging categories

COMPUTE maxiterations = 100. 
* default 100

COMPUTE converge = 0.001.
* default 0.001

COMPUTE intervals = 10. 
*default 10


USE ALL. 
COMPUTE filter=(uniform(1)<=.50). 
VARIABLE LABELS filter 'Approximately 50% of the cases (SAMPLE)'. 
FORMATS filter (f1.0). 
EXECUTE.

* Decision Tree. 
TREE clase [n] BY marketing_activo_ultimos90dias [n] cliente_vip [n] cliente_sucursal [s] cliente_edad [s] cliente_antiguedad [s] mrentabilidad [s] mrentabilidad_annual [s] mcomisiones [s] mactivos_margen [s] mpasivos_margen [s] marketing_coss_selling [n] tpaquete1 [n] tpaquete2 [n] tpaquete3 [n] tpaquete4 [n] tpaquete5 [n] tpaquete6 [n] tpaquete7 [n] tpaquete8 [n] tpaquete9 [n] tcuentas [n] tcuenta_corriente [n] mcuenta_corriente_Nopaquete [s] mcuenta_corriente_Paquete [s] mcuenta_corriente_dolares [n] 
tcaja_ahorro [n] mcaja_ahorro_Paquete [s] mcaja_ahorro_Nopaquete [s] mcaja_ahorro_dolares [s] mdescubierto_preacordado [s] mcuentas_saldo [s] ttarjeta_debito [n] ctarjeta_debito_transacciones [s] mautoservicio [s] ttarjeta_visa [n] ctarjeta_visa_transacciones [s] mtarjeta_visa_consumo [s] ttarjeta_master [n] ctarjeta_master_transacciones [s] mtarjeta_master_consumo [s] cprestamos_personales [s] mprestamos_personales [s] cprestamos_prendarios [n] mprestamos_prendarios [s] cprestamos_hipotecarios [n] 
mprestamos_hipotecarios [s] tplazo_fijo [n] mplazo_fijo_dolares [s] mplazo_fijo_pesos [s] tfondos_comunes_inversion [n] mfondos_comunes_inversion_pesos [s] mfondos_comunes_inversion_dolares [s] ttitulos [n] mtitulos [s] tseguro_vida_mercado_abierto [n] tseguro_auto [n] tseguro_vivienda [n] tseguro_accidentes_personales [n] tcaja_seguridad [n] mbonos_gobierno [n] mmonedas_extranjeras [n] minversiones_otras [n] tplan_sueldo [n] mplan_sueldo [s] mplan_sueldo_manual [s] cplan_sueldo_transaccion [n] 
tcuenta_debitos_automaticos [n] mcuenta_debitos_automaticos [s] ttarjeta_visa_debitos_automaticos [n] mttarjeta_visa_debitos_automaticos [s] ttarjeta_master_debitos_automaticos [n] mttarjeta_master_debitos_automaticos [s] tpagodeservicios [n] mpagodeservicios [s] tpagomiscuentas [n] mpagomiscuentas [s] ccajeros_propios_descuentos [s] mcajeros_propios_descuentos [s] ctarjeta_visa_descuentos [s] mtarjeta_visa_descuentos [s] ctarjeta_master_descuentos [s] mtarjeta_master_descuentos [s] ccuenta_descuentos [n] 
mcuenta_descuentos [n] ccomisiones_mantenimiento [n] mcomisiones_mantenimiento [s] ccomisiones_otras [s] mcomisiones_otras [s] tcambio_monedas [n] ccambio_monedas_compra [n] mcambio_monedas_compra [s] ccambio_monedas_venta [n] mcambio_monedas_venta [s] ctransferencias_recibidas [n] mtransferencias_recibidas [s] ctransferencias_emitidas [s] mtransferencias_emitidas [s] cextraccion_autoservicio [s] mextraccion_autoservicio [s] ccheques_depositados [s] mcheques_depositados [s] ccheques_emitidos [s] 
mcheques_emitidos [s] ccheques_depositados_rechazados [n] mcheques_depositados_rechazados [s] ccheques_emitidos_rechazados [n] mcheques_emitidos_rechazados [s] tcallcenter [n] ccallcenter_transacciones [s] thomebanking [n] chomebanking_transacciones [s] tautoservicio [n] cautoservicio_transacciones [s] tcajas [n] tcajas_consultas [n] tcajas_depositos [n] tcajas_extracciones [n] tcajas_otras [n] ccajeros_propio_transacciones [s] mcajeros_propio [s] ccajeros_ajenos_transacciones [s] mcajeros_ajenos [s] 
tmovimientos_ultimos90dias [n] Master_marca_atraso [n] Master_cuenta_estado [s] Master_mfinanciacion_limite [s] Master_Fvencimiento [s] Master_Finiciomora [s] Master_msaldototal [s] Master_msaldopesos [s] Master_msaldodolares [s] Master_mconsumospesos [s] Master_mconsumosdolares [s] Master_mlimitecompra [s] Master_madelantopesos [s] Master_madelantodolares [s] Master_fultimo_cierre [s] Master_mpagado [s] Master_mpagospesos [s] Master_mpagosdolares [s] Master_fechaalta [s] Master_mconsumototal [s] 
Master_tconsumos [n] Master_tadelantosefectivo [n] Master_mpagominimo [s] Visa_marca_atraso [n] Visa_cuenta_estado [s] Visa_mfinanciacion_limite [s] Visa_Fvencimiento [s] Visa_Finiciomora [s] Visa_msaldototal [s] Visa_msaldopesos [s] Visa_msaldodolares [s] Visa_mconsumospesos [s] Visa_mconsumosdolares [s] Visa_mlimitecompra [s] Visa_madelantopesos [s] Visa_madelantodolares [s] Visa_fultimo_cierre [s] Visa_mpagado [s] Visa_mpagospesos [s] Visa_mpagosdolares [s] Visa_fechaalta [s] Visa_mconsumototal [s] 
Visa_tconsumos [n] Visa_tadelantosefectivo [n] Visa_mpagominimo [s] 
  /TREE DISPLAY=TOPDOWN NODES=STATISTICS BRANCHSTATISTICS=YES NODEDEFS=YES SCALE=AUTO 
  /DEPCATEGORIES USEVALUES=[VALID] 
  /PRINT MODELSUMMARY CLASSIFICATION RISK CATEGORYSPECS TREETABLE 
  /SAVE PREDPROB 
  /METHOD TYPE=CHAID 
  /GROWTHLIMIT MAXDEPTH=AUTO MINPARENTSIZE=100 MINCHILDSIZE=50 
  /VALIDATION TYPE=SPLITSAMPLE(filter) OUTPUT=BOTHSAMPLES 
  /CHAID ALPHASPLIT=0.02 ALPHAMERGE=0.05 SPLITMERGED=NO CHISQUARE=PEARSON CONVERGE=0.001 MAXITERATIONS=100 ADJUST=BONFERRONI INTERVALS=intervals 
  /COSTS EQUAL 
  /MISSING NOMINALMISSING=MISSING.
EXECUTE.

IF  (clase='BAJA+2') ganancia=4900.
IF  (clase='BAJA+1') ganancia=-100.
IF  (clase='CONTINUA') ganancia=-100.
IF (PredictedProbability_2 > 0.02) g = ganancia.
CTABLES 
  /VLABELS VARIABLES=g DISPLAY=LABEL 
  /TABLE g [MEAN, SUM].
EXECUTE.


SORT CASES  BY filter. 
SPLIT FILE SEPARATE BY filter. 
ROC PredictedProbability_2 BY clase ('BAJA+2') 
  /CRITERIA=CUTOFF(INCLUDE) TESTPOS(LARGE) DISTRIBUTION(FREE) CI(95) 
  /MISSING=EXCLUDE.
EXECUTE.



