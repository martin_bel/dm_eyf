### Downsampling - modelos
rm(list = ls(all=TRUE)); gc()
setwd("/Users/mbtangotan/Desktop/dm/dm_eyf/caret_varios")
options(scipen=666)
pkg = c('data.table','foreign', 'nnet', 'ggplot2', 
        'caret', 'glmnet', 'doParallel', 'doMC')

sapply(pkg, require, c=T)
load('agg_basicas_201404.RData')
source('fun_modelar.R')
d <- preproc3(t1)
save(d, file='agg_basicas_201404_preproc.RData')


### Downsampling - modelos
rm(list = ls(all=TRUE)); gc()
setwd("/Users/mbtangotan/Desktop/dm/dm_eyf/caret_varios")
options(scipen=666)
pkg = c('data.table','foreign', 'nnet', 'ggplot2', 
        'caret', 'glmnet', 'doParallel', 'doMC')

sapply(pkg, require, c=T)
load('agg_basicas_201404.RData')
source('fun_modelar.R')
d <- preproc4(t1)
save(d, file='agg_basicas_201404_preproc4.RData')
