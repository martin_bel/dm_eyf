options(scipen=666)
rm(list = ls(all=TRUE))
setwd("/Users/mbtangotan/Desktop/dm/dm_eyf/agg/modelos")
source('fun_modelar.R')

pkg = c('foreign', 'nnet', 'ggplot2', 'caret', 'glmnet', 
        'doParallel', 'doMC', 'svmpath', 'LiblineaR', 'data.table')
sapply(pkg, require, c=T)

# Para procesar en paralelo
registerDoParallel(4)
#registerDoMC(cores = 4)

load('preproc_agg_201403.RData')
rm(list = c('x_train', 'x_test', 'y_test', 'y_train', 'predictors', 'ctrl'))
gc()

drop_nzr = read.table('nzv.txt', stringsAsFactors=F)[,1]
drop_corr = read.table('corr.txt', stringsAsFactors=F)[,1]

d <- d[ , names(d)[!names(d) %in% c(drop_nzr, drop_corr)]]

set.seed(1)
predictors <- names(d)[!names(d) %in% c("clase","id_numero_de_cliente")]
inTrainingSet <- createDataPartition(d$clase, p = 0.7, list = FALSE)

train <- d[inTrainingSet, ]
test <- d[-inTrainingSet, ]

y_train = d[inTrainingSet, 'clase']
y_test = d[-inTrainingSet, 'clase']

ln <- apply(test, 2, function(x) length(unique(x)))
num <- names(ln[ln > 30])
fact <- names(ln[ln < 4])
fact <- fact[!fact %in% c('clase')]
resto <- names(train)[!names(train) %in% c(num, fact, 'clase')]

centerScale <- preProcess(train[, num])
train_num <- predict(centerScale, train[, num])
test_num <- predict(centerScale, test[, num])

train <- data.frame(cbind(train_num, train[, fact], train[, resto]))
test <- data.frame(cbind(test_num, test[, fact], test[, resto]))

d <- rbind(train, test)
x_all = model.matrix(~ . -1, data = d[, predictors])
inx_train <- 1:nrow(train)
inx_test <- (nrow(train) + 1):(nrow(d))

x_train = x_all[inx_train,]
x_test <- x_all[inx_test, ]

rm(list = c('train_num', 'test_num', 'train', 'x_all', 'x_test', 'd', 'test'))
gc()

fit <- knnreg(x_train, y_train, k = 3)
plot(testY, predict(fit, testX))

Sys.time()
ctrl <- trainControl(
  method='cv', number = 3, repeats=1, classProbs=TRUE, 
  summaryFunction = twoClassSummary
)

nnet <- train(
  x_train, y_train, method = "nnet", metric='ROC',
  trControl = ctrl, tunelength=5
)


# save(nnet, file='2_nnet_201403_80p.RData')
# load('1_nnet_201403_80p.RData')


pred <- predict(nnet, x_test, type = "prob")

pred <- ifelse(pred[,1] > 0.02, 'BAJA', 'CONTINUA')
confusionMatrix(pred, y_test)

tbl <- table(pred, y_test)
ganancia(tbl, mult=5)

cut_off <- function(cut, pred, clase_test, mult=1){
  response = ifelse(pred > cut, 'BAJA', 'CONTINUA')
  tbl = table(response, clase_test)
  gan = tbl[1,1] * 5000 - (sum(tbl[1,]) * 100)
  gan = gan * mult
  print(sprintf('Ganancia: %s Cut off: %s', gan, cut))
  list(gan=gan, tbl=tbl, response=response, probs=pred)
}

ct = lapply(seq(0.02, 0.025, 0.0001), function(x) cut_off(x, pred[,1], y_test, mult=5))

rocCurve <- roc(
  response = as.factor(clase_bin),
  predictor = gbmProbs[, "BAJA2"],
  levels = rev(levels(as.factor(clase_bin)))
)
