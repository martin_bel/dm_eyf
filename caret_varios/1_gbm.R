options(scipen=666)
rm(list = ls(all=TRUE))
load("agg_201403.RData")

setwd("/Users/mbtangotan/Desktop/dm/dm_eyf/agg/modelos")
source('fun_modelar.R')

pkg <- c('caret', 'rpart', 'pROC', 'doMC', 'Hmisc', 'gbm')
sapply(pkg, require, c=T)

### ADA-BOOST ### CARET ###
# d <- preproc1(d)
### Prueba con menos casos
# train <- train[1:10000, ]

load('preproc_agg_201403.RData')
rm(list = c('x_train', 'x_test', 'y_test', 'y_train', 'predictors', 'ctrl'))
gc()


set.seed(1)
predictors <- names(d)[!names(d) %in% c("clase","id_numero_de_cliente")]
inTrainingSet <- createDataPartition(d$clase, p = 0.2, list = FALSE)

train <- d[inTrainingSet, ]
test <- d[-inTrainingSet, ]

### Near zero variance
nzv <- nearZeroVar(train)
write.table(names(train[1, nzv[-length(nzv)] ] ), 'nzv.txt', row.names=F)
nzv <- nzv[-length(nzv)]
train <- train[, -nzv]
test <- test[, -nzv]

gc()

### Elimino variables correlacionadas
tipo_dato <- td(train)
corrMatrix <- cor(train[, tipo_dato[['num']] ])
corr_vars <- findCorrelation(corrMatrix, cutoff = .80, verbose = F)
# write.table(names(train[1, corr_vars] ), 'corr.txt', row.names=F)
train <- train[, -corr_vars]
test <- test[, -corr_vars]

rm(corrMatrix)
d <- rbind(train, test)

predictors <- names(d)[!names(d) %in% c("clase","id_numero_de_cliente")]
x_all = model.matrix(~ . -1, data = d[, predictors])
inx_train <- 1:nrow(train)
inx_test <- (nrow(train) + 1):(nrow(d))

y_train = d[inx_train, 'clase']
y_test = d[inx_test, 'clase']
x_train = x_all[inx_train,]
x_test <- x_all[inx_test, ]


# rm(d)
gc()

# save.image('preproc_agg_201403.RData')

vars_train <- names(x_train)
write.table(vars_train, 'vars_train_gbm.txt', row.names=F)


rm(list=c('d', 'train', 'test'))
gc()

Sys.time()
registerDoMC(cores = 4)
ctrl <- trainControl(method = "cv", number=3, repeats=1, classProbs=TRUE)

gbmGrid <-  expand.grid(interaction.depth = c(1, 5, 9),
                        n.trees = c(50, 100, 250, 500, 1000),
                        shrinkage = c(0.1))

tune <- train(x = x_train, y = y_train,
  method = 'gbm', verbose = FALSE, tuneGrid = gbmGrid,
  trControl = ctrl, metric = 'ROC'
)
Sys.time()

### Predicciones

pred <- predict(tune, x_test, type = "prob")
cuts <- lapply(seq(0,1,0.1), function(x) cut_off(x, pred, y_test, mult=1.428571))
cuts <- lapply(seq(0,0.3,0.01), function(x) cut_off(x, pred, y_test, mult=1.428571))


ganancia(tbl)

rocCurve <- roc(
  response = as.factor(clase_bin),
  predictor = gbmProbs[, "BAJA2"],
  levels = rev(levels(as.factor(clase_bin)))
)

#rocCurve <- roc(
#  controls = clase_bin,
#  cases = gbmProbs[, "BAJA2"]
#)

plot(rocCurve)
