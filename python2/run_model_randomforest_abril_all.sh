criterion="gini entropy"
split_seed="000 312 652 829 192 431 232 592"
max_depth="3 5 10 20"
min_samples_split="3 10 20 50 100"
min_samples_leaf="1 10 20 50 100"
iterations="10 20 50 100 200"

#max_features = auto
#max_leaf_nodes = None

echo "gain\titerations\tcriterion\tdepth\tmin split\tmin leaf\tseed" > run_model_randomforest_abril_all.out
for s in `echo $split_seed`; do
for i in `echo $iterations`; do
for c in `echo $criterion`; do
for d in `echo $max_depth`; do
for ms in `echo $min_samples_split`; do
for ml in `echo $min_samples_leaf`; do
    result=`python run_model.py --data_file abril_2014.txt --split_seed $s --criterion $c --max_depth $d --min_samples_split $ms --min_samples_leaf $ml --randomforest true --iterations $i`
    gain=`echo $result|tail -n 1|cut -d ':' -f 2`
    echo "$gain\t$i\t$c\t$d\t$ms\t$ml\t$s" >> run_model_randomforest_abril_all.out
done
done
done
done
done
done