print(__doc__)

import numpy as np
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeRegressor
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.cross_validation import cross_val_score
from sklearn.cross_validation import train_test_split

df = pd.read_csv("abril_2014.python.txt", delimiter="\t")

clase = df['clase']
del df['clase']
df = df.fillna(0)


# Fit regression model
#dt = DecisionTreeRegressor(max_depth=2)
#dt.fit(df, clase)

#http://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html
#max_features, max_depth,min_samples_split,min_samples_leaf,max_leaf_nodes, criterion='entropy'
#random_state = seed
clf = RandomForestClassifier(n_estimators=100, n_jobs=-1)
clf = clf.fit(df, clase)

scores = cross_val_score(clf, df, clase)

# Predict
X_test = df
pred = dt.predict(X_test)

print pred