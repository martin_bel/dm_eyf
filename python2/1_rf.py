import pandas
import numpy
import sys
import re
import json
import argparse
import matplotlib.pyplot as plt

from sklearn.grid_search import GridSearchCV
from sklearn import tree
from sklearn.metrics import roc_curve, auc
from sklearn.preprocessing import label_binarize
from sklearn.multiclass import OneVsRestClassifier
from sklearn.cross_validation import train_test_split
from sklearn.utils import array2d

import numpy as np
from sklearn.ensemble import RandomForestClassifier

pandas.options.mode.chained_assignment = None

MOST_MIN = -149849333.41
NAN_REPLACE = -99999999999999


d = pandas.read_csv(
  'data_agg_201404_40pct.txt.python',
  sep='\t',
  index_col=False,
  low_memory=False,
  header=0
)

mins = []
for c in cols:
    if d[c].dtype == 'object':
        d[c] = d[c].apply(lambda x : 0 if x.upper() == 'N' else 1)
    elif d[c].dtype == 'float64':
        d[c] = d[c].fillna(NAN_REPLACE)
        mins.append(d[c].min())

# create a df without the class column
cols = [col for col in d.columns if col != 'clase']
df = d[cols]

X_train, X_test, y_train, y_test = train_test_split(
            df, d['clase'],
            test_size=0.5,
            random_state=0)

del d

# clf = RandomForestClassifier(n_estimators=100, n_jobs=-1)


def ganancia(predicted_proba=predicted_proba, 
  clase=y_test) :
  pdf = pandas.DataFrame(
    {'prob': predicted_proba, 'class': y_test})
  gan = sum(pdf.apply(lambda x : (4900 if x.get('class') == 'BAJA+2' else -100) if x.get('prob') > 0.02 else 0, axis=1))
  gan = gan * 5
  print(gan)
  return(gan)



clf = RandomForestClassifier(n_estimators=250,
  min_samples_split=10,
  min_samples_leaf=10,
  n_jobs=-1)

clf = clf.fit(X_train, y_train)
predicted_proba = clf.predict_proba(X_test)[:,1]
ganancia()

pdf = pandas.DataFrame(
    {'prob': predicted_proba, 'class': y_test})

sum(pdf.apply(lambda x : (4900 if x.get('class') == 'BAJA+2' else -100) if x.get('prob') > 0.02 else 0, axis=1))


parameters = {'kernel':('linear', 'rbf'), 'C':[1, 10]}
svr = svm.SVC()
clf = grid_search.GridSearchCV(svr, parameters)
clf.fit(iris.data, iris.target)


### GRID SEARCH CV ###

tuned_parameters = [{'max_features': ['sqrt', 'log2'], 'n_estimators': [100, 200, 500]}]

rf = GridSearchCV(
  RandomForestClassifier(
    min_samples_split=1000, 
    n_jobs=-1), 
  tuned_parameters, 
  cv=3, verbose=2 
  ).fit(X_train, y_train
)

m = rf.fit(X_train,  y_train)
predicted_proba = m.predict_proba(X_test)[:,1]

pdf = pandas.DataFrame(
    {'prob': predicted_proba, 'class': y_test})

sum(pdf.apply(lambda x : (4900 if x.get('class') == 'BAJA+2' else -100) if x.get('prob') > 0.02 else 0, axis=1))



