import pandas
import numpy
import sys
import re
import json
import argparse
import matplotlib.pyplot as plt

from sklearn import tree
from sklearn.metrics import roc_curve, auc
from sklearn.preprocessing import label_binarize
from sklearn.multiclass import OneVsRestClassifier
from sklearn.cross_validation import train_test_split
from sklearn.utils import array2d

import numpy as np
from sklearn.ensemble import RandomForestClassifier

pandas.options.mode.chained_assignment = None

MOST_MIN = -149849333.41
NAN_REPLACE = -99999999999999

april = pandas.read_csv(
  'train_agg_201404_3.txt',
  sep='|',
  index_col=False,
  # low_memory=False,
  header=0,
  engine='python'
)


# create a df without the class column
cols = [col for col in april.columns if col != 'clase']
april_data = april[cols]

mins = []
for c in cols:
    if april_data[c].dtype == 'object':
        april_data[c] = april_data[c].apply(lambda x : 0 if x.upper() == 'N' else 1)
    elif april_data[c].dtype == 'float64':
        april_data[c] = april_data[c].fillna(NAN_REPLACE)
        mins.append(april_data[c].min())

X_train, X_test, y_train, y_test = train_test_split(
            april_data, april['clase'],
            test_size=0.3,
            random_state=0)


clf = RandomForestClassifier(n_estimators=2, n_jobs=-1)

clf = clf.fit(X_train, y_train)

predicted_proba = clf.predict_proba(X_test)[:,1]

pdf = pandas.DataFrame(
    {'prob': predicted_proba, 'class': y_test})

sum(pdf.apply(lambda x : (4900 if x.get('class') == 'BAJA+2' else -100) if x.get('prob') > 0.02 else 0, axis=1))
