import pandas
import numpy
import sys
import re
import json
import argparse
import matplotlib.pyplot as plt

from sklearn.grid_search import GridSearchCV
from sklearn import tree
from sklearn.metrics import roc_curve, auc
from sklearn.preprocessing import label_binarize
from sklearn.multiclass import OneVsRestClassifier
from sklearn.cross_validation import train_test_split
from sklearn.utils import array2d

import numpy as np
from sklearn.ensemble import RandomForestClassifier

pandas.options.mode.chained_assignment = None

MOST_MIN = -149849333.41
NAN_REPLACE = -99999999999999


d = pandas.read_csv(
  'data_agg_201404_seed10_40.txt.python',
  sep='\t',
  index_col=False,
  low_memory=False,
  header=0
)

# create a df without the class column
cols = [col for col in d.columns if col != ['clase', 'mes'] ]
df = d[cols]

mins = []
for c in cols:
    if df[c].dtype == 'object':
        df[c] = df[c].apply(lambda x : 0 if x == 'N' else (1 if x == 'S' else (0)))
        # df[c] = df[c].apply(lambda x : 0 if x.upper() == 'N' else 1)
    elif df[c].dtype == 'float64':
        df[c] = df[c].fillna(NAN_REPLACE)
        mins.append(df[c].min())


X_train, X_test, y_train, y_test = train_test_split(
            df, d['clase'],
            test_size=0.5,
            random_state=0)

del d

# clf = RandomForestClassifier(n_estimators=100, n_jobs=-1)


def ganancia(predicted_proba, y_test, mult=5, cut=0.02, print_it=False) :
  pdf = pandas.DataFrame(
    {'prob': predicted_proba, 'class': y_test})
  gan = sum(pdf.apply(lambda x : (4900 if x.get('class') == 'BAJA+2' else -100) if x.get('prob') > cut else 0, axis=1))
  gan = gan * mult
  if print_it == True:
    print(gan)
  return(gan)

def cut_off():
  from scipy import arange
  for i in arange(0, 1, 0.001):
    print ganancia(predicted_proba, y_test, 5, i), i


### Un Random Forest - Va ok
clf = RandomForestClassifier(n_estimators=250,
  min_samples_split=10,
  min_samples_leaf=10,
  n_jobs=-1)

clf = clf.fit(X_train, y_train)
predicted_proba = clf.predict_proba(X_test)[:,1]
ganancia(predicted_proba, y_test, 5)
# 2313000

cut_off()