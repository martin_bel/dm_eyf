criterion="gini entropy"
split_seed="000 652 829 192 431 232 592" #000 312
max_depth="10 20 25"
min_samples_split="20 50 100"
min_samples_leaf="20 50 100"
iterations="150 10 20 50 200"

#max_features = auto
#max_leaf_nodes = None

#echo "gain\titerations\tcriterion\tdepth\tmin split\tmin leaf\tseed" > run_model_randomforest_abril_all.out


for s in `echo $split_seed`; do
for i in `echo $iterations`; do
for c in `echo $criterion`; do
for d in `echo $max_depth`; do
for ms in `echo $min_samples_split`; do
for ml in `echo $min_samples_leaf`; do

        ./pause.sh /tmp/run_randomforest_agg

        file="run_randomforest_agg_${c}_${d}_${ms}_${ml}_${i}_${s}.probs"
        result=`python run_model2.py --data_file ../data/data_agg_con_tendencia_201404_I.txt.python --data_file_size 5 -cv true --split_seed $s --criterion $c --max_depth $d --min_samples_split $ms --min_samples_leaf $ml --iterations $i `
	#--print_probs probs/$file
        
	gain=`echo $result|grep "Norm test total gain"|tail -n 1|cut -d ':' -f 2`
        echo "$gain\t$file" >> run_model_randomforest_abril_agg_all.out
    done
done
done
done
done
done
done
