import pandas
import numpy
import sys
import re
import json
import argparse
import matplotlib.pyplot as plt

from sklearn import tree
from sklearn.metrics import roc_curve, auc
from sklearn.preprocessing import label_binarize
from sklearn.multiclass import OneVsRestClassifier
from sklearn.cross_validation import train_test_split
from sklearn.utils import array2d

import numpy as np
from sklearn.ensemble import RandomForestClassifier

pandas.options.mode.chained_assignment = None

MOST_MIN = -149849333.41
NAN_REPLACE = -99999999999999

april = pandas.read_csv('../data/data_agg_201404_1.txt.python', sep='\t', low_memory=False, header=0)

cols = [col for col in april.columns if col != 'clase']

april_data = april[cols]

X_train, X_test, y_train, y_test = train_test_split(april_data, april['clase'],test_size=0.3,random_state=0)


X_train = array2d(X_train, dtype=np.float32)


file="../data/data_simple_201404_1.txt.python"
file="../data/data_agg_con_tendencia_201404_1.txt.python"
d = pandas.read_csv(file, sep='\t', low_memory=False, header=0)
X_train = transform(d)
y_train = d['clase']


clf = tree.DecisionTreeClassifier(max_depth=5,criterion="gini",min_samples_split=5,min_samples_leaf=5)
clf = RandomForestClassifier(n_estimators=2)

clf = clf.fit(X_train, y_train)
tree.export_graphviz(clf, out_file="/tmp/a.dot")

clf.feature_importances_
cols = [col for col in d.columns if col != 'clase']
features = pandas.DataFrame({'columns':cols, 'f':clf.feature_importances_})
features = features.sort("f", ascending=False)


def transform(df):
    # create a df without the class column
    cols = [col for col in df.columns if col != 'clase']
    #print(col)
    df_data = df[cols]
    # transform s/n cols to 0/1 columns
    # transform NAN in numeric columns to a known numeric value so that
    # the tree won't complain on cast time
    #print 'working on data ...'
    mins = []
    for c in cols:
        if df_data[c].dtype == 'object':
            df_data[c] = df_data[c].apply(lambda x : 0 if x == 'N' else (1 if x == 'S' else (0 if x == c else float(x))))
    for c in cols:
        if df_data[c].dtype == 'float64':
            df_data[c] = df_data[c].fillna(NAN_REPLACE)
            mins.append(df_data[c].min())
    return df_data
