import pandas
import numpy
import sys
import re
import json
import argparse
import matplotlib.pyplot as plt

from sklearn import tree
from sklearn.metrics import roc_curve, auc
from sklearn.preprocessing import label_binarize
from sklearn.multiclass import OneVsRestClassifier
from sklearn.cross_validation import train_test_split
from sklearn.utils import array2d

import numpy as np
from sklearn.ensemble import RandomForestClassifier

pandas.options.mode.chained_assignment = None

NAN_REPLACE = -99999999999999
MOST_MIN = -149849333.41

from datetime import datetime

def get_leafs(dot_filename):
    with open(dot_filename, 'r') as f:
        leafs = [line.strip() for line in f if line.find('value') != -1]
    res = []
    for l in leafs:
        #p = re.compile('nodeid\s=\s[\d]+')
        p = re.compile('^[\d]+')

        begin, end = p.search(l).span()
        node_id = int(l[begin:end]) #int(l[begin:end].split('=')[1].strip())
        p = re.compile('samples\s=\s[\d]+')

        begin, end = p.search(l).span()
        total = int(l[begin:end].split('=')[1].strip())
        p = re.compile('value\s+=\s+\[[\d\s\.]+\]')

        if p.search(l) is not None:
            begin, end = p.search(l).span()
            vals = [int(i.strip('.')) for i in l[begin:end].strip('value = []').split()]
            assert(total == sum(vals))
        else:
            vals = []

        res.append({'node_id': node_id, 'value': vals, 'probs':[]})
    return res

def get_leaf_gain(leaf):
    baja1 = 0
    baja2 = 0
    continua = 0
    if 'BAJA+2' in leaf:
        baja2 = leaf['BAJA+2']
    if 'BAJA+1' in leaf:
        baja1 = leaf['BAJA+1']
    if 'CONTINUA' in leaf:
        continua = leaf['CONTINUA']
    return (5000 * baja2) - (100 * (baja1 + baja2 + continua))
    
def get_gain(baja_1, baja_2, cont):
    return (5000 * baja_2) - (100 * (baja_1 + baja_2 + cont))

def args_to_json(args):
    p = {}
    p['test_ratio'] = args.test_ratio
    p['split_seed'] = args.split_seed
    p['criterion'] = args.criterion
    p['max_depth'] = args.max_depth
    p['min_samples_split'] = args.min_samples_split
    p['min_samples_leaf'] = args.min_samples_leaf
    p['splitter'] = args.splitter
    p['add_columns'] = args.add_columns
    return json.dumps(p)

def columns(columns):
    a = ['max','min','dif','cou','sum','mea']
    cols = [col for col in columns if col != 'clase' and col[0:3] not in a]
    return cols

def transform(df):
    # create a df without the class column
    cols = columns(df.columns)

    #print(col)
    df_data = df[cols]
    # transform s/n cols to 0/1 columns
    # transform NAN in numeric columns to a known numeric value so that
    # the tree won't complain on cast time
    #print 'working on data ...'
    mins = []
    for c in cols:
        if df_data[c].dtype == 'object':
            df_data[c] = df_data[c].apply(lambda x : 0 if x == 'N' else (1 if x == 'S' else (0 if x == c else float(x))))
            #df_data[c] = df_data[c].apply(lambda x : 0 if x.upper() == 'N' else (1 if x.upper() == 'S' else (0 if x == c else float(x))))
        if df_data[c].dtype == 'float64':
            df_data[c] = df_data[c].fillna(NAN_REPLACE)
            mins.append(df_data[c].min())

        #if df_data[c].dtype == 'object':
        #    print c
        #    print df_data[c]




    return df_data

def readSource(fileName, size, n):
    data = []
    t1 = datetime.now()
    if n:
        for i in n:
            file = fileName.replace("I", i)
            if args.verbose:
                print "reading file %s " % file
            d = pandas.read_csv(file, sep='\t', low_memory=False, header=0)
            if args.verbose:
                print "size %d " % len(d)
            data.append(d)

    else:

        for i in range(0,size):
            file = fileName.replace("I", str(i+1) )
            if args.verbose:
                print "reading file %s " % file
            d = pandas.read_csv(file, sep='\t', low_memory=False, header=0)
            data.append(d)

    if args.verbose:
        print "Time reading ", (datetime.now()-t1).seconds

    #train = pandas.read_csv(args.data_file, sep='\t', low_memory=False, header=0)
    return data

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
            description='decision tree evaluator.')
    parser.add_argument('-d', '--dot_file', type=str, default='trees/tree.dot', help='output dot file')

    parser.add_argument('--data_file', type=str, help='train data file')
    parser.add_argument('--data_file_size', type=int, default=1, help='train data file size')
    parser.add_argument('--data_file_n', type=str, nargs="+", help='train data file numbers (list of partitions)')

    parser.add_argument('--test_file', type=str, help='test data file')
    parser.add_argument('--test_file_size', type=int, default=1, help='test data file size')
    parser.add_argument('--test_file_n', type=str, nargs="+", help='test data file numbers (list of partitions)')

    parser.add_argument('-t', '--test_ratio', type=float, default=1, help='test set portion')
    parser.add_argument('-cv', '--cross_validation', type=str, help='cross validation')

    parser.add_argument('-save', '--save_model', type=str, help='save model')
    parser.add_argument('-load', '--load_model', type=str, help='load model')

    parser.add_argument('-s', '--split_seed', type=int, default=0, help='seed for the shuffle function')
    parser.add_argument('-c', '--criterion', type=str, choices=['gini','entropy'], default='gini',
                                help='split criterion')
    parser.add_argument('-m', '--max_depth', type=int, default=3, help='max depth for decision tree')
    parser.add_argument('-i', '--min_samples_split', type=int, default=3, help='min samples for split')
    parser.add_argument('-l', '--min_samples_leaf', type=int, default=1, help='min samples on a leaf')
    parser.add_argument('-p', '--splitter', type=str, choices=['best', 'random'], default='best', 
                                    help='startegy to choose the split at each node')
    parser.add_argument('-r', '--print_probs', type=str, help='print prob b2 to csv file')
    parser.add_argument('-a', '--add_columns', type=str, nargs='+', help='adds colls from csv file')
    parser.add_argument('-o', '--roc_curve',type=str, help='generate roc curve')
    parser.add_argument('-v', '--verbose',type=str, help='print debug info')
    parser.add_argument('-e', '--iterations',type=int,default=0,help='number of trees in random forest')
    args = parser.parse_args()
    if args.verbose:
        print args

    t1 = datetime.now()

    if args.load_model:
        #load model
        print 'Loading model from ', args.load_model
        from sklearn.externals import joblib
        clf = joblib.load(args.load_model)

        #read test
        data = readSource(args.test_file, args.test_file_size, args.test_file_n)

        test = None
        for i in range(0,len(data)):
            test = data[i] if test is None else test.append(data[i])

        X_test = transform(test)
        y_test = test['clase']

        #predictions
        predicted_proba = clf.predict_proba(X_test)[:,1]
        pdf = pandas.DataFrame({'prob': predicted_proba, 'class': y_test})
        test_gain = sum(pdf.apply(lambda x : (4900 if x.get('class') == 'BAJA+2' else -100) if x.get('prob') > 0.02 else 0, axis=1))

        size_data = len(X_test)
        size_test = len(X_test)
        baja2_count = (y_test == "BAJA+2").sum()

    else:
        # read train
        data = readSource(args.data_file, args.data_file_size, args.data_file_n)

        if args.cross_validation:
            if args.verbose:
                print "ignorar"
            #train = transform(data)
            #clase = data['clase']

            #from sklearn import cross_validation
            #kf = cross_validation.KFold(len(data), n_folds=5, random_state=args.split_seed)

        else:
            #append all datasets
            train = None
            for i in range(0,len(data)):
                train = data[i] if train is None else train.append(data[i])

            X_train = transform(train)
            y_train = train['clase']

            # read test
            if args.test_file:
                data_test = readSource(args.test_file, args.test_file_size, args.test_file_n)

                test = None
                for i in range(0,len(data_test)):
                    test = data_test[i] if test is None else test.append(data_test[i])

                X_test = transform(test)
                y_test = test['clase']
            else:
                if args.verbose:
                    print 'splitting train and test %f ' % args.test_ratio
                X_train, X_test, y_train, y_test = train_test_split(
                        X_train, y_train,
                        test_size=args.test_ratio,
                        random_state=args.split_seed)

    # add columns to dataset
    #TODO
    if args.add_columns:
        for col in args.add_columns:
            if args.verbose:
                print 'adding column form file %s' % col
            april_data[col.split('.')[0]] = numpy.genfromtxt(col, delimiter=',')

    #assert(NAN_REPLACE == min(mins))
    #print 'training ... '
    # split the data set
    #X_train, X_test, y_train, y_test = train_test_split(
    #            april_data, april['clase'],
    #            test_size=args.test_ratio,
    #            random_state=args.split_seed)

    if args.iterations == 0:
        if args.verbose:
            print 'DecisionTree '
        clf = tree.DecisionTreeClassifier(
                max_depth=args.max_depth,
                criterion=args.criterion,
                min_samples_split=args.min_samples_split,
                min_samples_leaf=args.min_samples_leaf,
                splitter=args.splitter)
    else:
        if args.verbose:
            print 'RandomForest %d iterations' % args.iterations
        clf = RandomForestClassifier(n_estimators=args.iterations, n_jobs=-1,
                 max_depth=args.max_depth,
                criterion=args.criterion,
                min_samples_split=args.min_samples_split,
                min_samples_leaf=args.min_samples_leaf,
                random_state=args.split_seed,
                max_features="auto",max_leaf_nodes=None,bootstrap=True,oob_score=False,min_density=None,compute_importances=None)
        #max_features="auto"

    if args.cross_validation:
        if args.verbose:
            print "Cross-validation, total folds %d " % len(data)

        #usando scikit learn -> no funciona
        #for train_index, test_index in kf:
        #    print("TRAIN:", train_index, "TEST:", test_index)
        #    print "MAX MIN ", train_index.max(), train_index.min()
        #    print "MAX MIN ", test_index.max(), test_index.min()
        #    X_train = train[train_index]
        #    X_test = train[test_index]
        #    y_train, y_test = clase[train_index], clase[test_index]

        #cross validation casero
        sum_test_gain = 0
        sum_count_test_baja2 = 0
        sum_size_test = 0
        sum_size_data = 0

        for i in range(0,len(data)):
            train = None
            for j in range(0,len(data)):
                if i != j:
                    if train is None:
                        train = data[j]
                    else:
                        train = train.append(data[j])
                else:
                    test = data[j]

            X_train = transform(train)
            y_train = train['clase']

            X_test = transform(test)
            y_test = test['clase']

            if args.verbose:
                print "Fold ", i
                print "Train size ", len(train)
                print "Test size ", len(test)
                print "Baja2 count ", (test.clase == "BAJA+2").sum()

            #predictions
            clf = clf.fit(X_train, y_train)
            predicted_proba = clf.predict_proba(X_test)[:,1]

            pdf = pandas.DataFrame({'prob': predicted_proba, 'class': y_test})

            test_gain = sum(pdf.apply(lambda x : (4900 if x.get('class') == 'BAJA+2' else -100) if x.get('prob') > 0.02 else 0, axis=1))
            sum_test_gain += test_gain

            count_test_baja2 = (test.clase == "BAJA+2").sum()
            sum_count_test_baja2 += count_test_baja2

            sum_size_test += len(test)
            sum_size_data += len(train) + len(test)

            if args.verbose:
                print 'Test total gain : %d' % test_gain

        test_gain = sum_test_gain / len(data)

        size_data = sum_size_data / len(data)
        size_test = sum_size_test / len(data)
        baja2_count = sum_count_test_baja2 / len(data)

        if args.verbose:
            print 'Avg data size: %5.3f' % size_data
            print 'Avg test size: %5.3f' % size_test
            print 'Avg baja2 count: %5.3f' % baja2_count

    else:
        #predictions
        clf = clf.fit(X_train, y_train)
        predicted_proba = clf.predict_proba(X_test)[:,1]

        if args.iterations:
            i = 1
            for t in clf.estimators_:
                tree.export_graphviz(t, out_file=args.dot_file + str(i), feature_names=columns(train.columns))
                i=i+1

            #cols = [col for col in train.columns if col != 'clase']
            features = pandas.DataFrame({'columns':columns(train.columns), 'f':clf.feature_importances_})
            features = features.sort("f", ascending=False)
            features.to_csv("/tmp/features")

        else:
            tree.export_graphviz(clf, out_file=args.dot_file, feature_names=train.columns)

        pdf = pandas.DataFrame({'prob': predicted_proba, 'class': y_test})

        test_gain = sum(pdf.apply(lambda x : (4900 if x.get('class') == 'BAJA+2' else -100) if x.get('prob') > 0.02 else 0, axis=1))

        if args.verbose:
            print 'Test total gain : %d' % test_gain

        size_data = len(X_train) + len(X_test)
        size_test = len(X_test)
        baja2_count = (y_test == "BAJA+2").sum()

    if args.test_file: # si hay test_file entonces no tiene sentido el ratio
        ratio = 1
    else:
        #if args.cross_validation:
        ratio = size_test * 1.0 / size_data # ratio calculado, deberia ser igual al pasado por parametro si es que se paso
        #else:
        #    ratio = size_test * 1.0 / (size_test+size_data) # ratio calculado, deberia ser igual al pasado por parametro si es que se paso

    test_gain_norm = int(test_gain / ratio)
    #test_gain_norm = test_gain

    if args.verbose:
        print "Time total", ((datetime.now()-t1).seconds / 60.0)

    print 'Test size %5.3f' % (size_test)
    print 'Data size %5.3f' % (size_data)
    print 'Test Baja+2 count %5.3f' % (baja2_count)
    print 'Norm test total gain %5.3f: %d' % ( ratio , test_gain_norm )

    if args.save_model:
        print 'Saving model to ', args.save_model
        import pickle
        s = pickle.dumps(clf)
        from sklearn.externals import joblib
        joblib.dump(clf, args.save_model)

    if args.roc_curve:
        pdf['bin_class'] = pdf.apply(lambda x : 1 if x.get('pclass') == 'BAJA+2' else 0, axis=1)
        fpr, tpr, thresholds = roc_curve(list(pdf['bin_class']), predicted_proba[:,1])
        roc_auc = auc(fpr, tpr)
        print 'Area under the ROC curve : %f' % roc_auc

        plt.clf()
        plt.plot(fpr, tpr, label='ROC curve (area = %0.2f)' % roc_auc)
        plt.plot([0, 1], [0, 1], 'k--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.0])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('Receiver operating characteristic')
        plt.legend(loc="lower right")
        plt.savefig('roc.png')
        plt.close()

    if args.print_probs :
        if args.verbose:
            print 'calculate B2 probs for dataset and dump to %s' % args.print_probs
        #predicted_proba = clf.predict_proba(april_data)
        #numpy.savetxt(args.print_probs, predicted_proba[:,1])
        #numpy.savetxt(args.print_probs + ".ids", numpy.array(X_test[:,0],dtype=numpy.integer))
        #pdf = pandas.DataFrame({'id': numpy.array(X_test[:,0],dtype=numpy.integer), 'prob': predicted_proba, 'class': y_test, 'ganancia': test_gain_norm})
        pdf = pandas.DataFrame({'id': X_test.numero_de_cliente, 'prob': predicted_proba, 'class': y_test, 'ganancia': test_gain_norm})
        pdf.to_csv(args.print_probs + ".csv")





#CALCULO DE NODOS HOJA PARA UN ARBOL
        #     leafs = get_leafs(args.dot_file)
        #     # create a list of leafs with the ids and assigned values
        #     leaf_maps = [{
        #               "value": dict(zip(list(clf.classes_), list(leaf['value']))),
        #               "node_id": leaf['node_id']
        #                } for leaf in leafs]
        #     gains = []
        #     for leaf in leaf_maps:
        #         gain = get_leaf_gain(leaf['value'])
        #         gains.append((leaf['node_id'], gain))
        #     train_gain = sum([gain[1] for gain in gains if gain[1] > 0])
        #
        #     # save the nodes id with positive gain
        #     positive_nodes = [gain[0] for gain in gains if gain[1] > 0]
        #     # some black magic casting in order to pass the
        #     # data set into the underlying implementation
        #     #X = array2d(X_test, dtype=clf.DTYPE)
        #     X = array2d(X_test, dtype=np.float32)
        #     #X = X_test
        #     #print 'predicting nodes ...'
        #     predicted_nodes = clf.tree_.apply(X)
        #
        #     # create a dataframe
        #     pdf = pandas.DataFrame(
        #                 {'node_id': predicted_nodes, 'pclass': y_test})
        #
        #     res = []
        #     for nid in positive_nodes:
        #         df = pdf[pdf.node_id == nid]
        #         baja_1 = len(df[df.pclass == 'BAJA+1'])
        #         baja_2 = len(df[df.pclass == 'BAJA+2'])
        #         cont   = len(df[df.pclass == 'CONTINUA'])
        #         g = get_gain(baja_1, baja_2, cont)
        #         res.append(g)
        # #        print 'Node = %.3d -> B1 = %.3d , B2 = %.3d , C = %.5d , gain = %.7d' % (
        # #                    nid, baja_1, baja_2, cont, g)
        #
        #     test_gain = sum(res)
