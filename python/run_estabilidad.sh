criterion="gini entropy"
split_seed="000 652 829 192 431 232 592" #000 312
max_depth="3 5 10 20"
min_samples_split="5 10 20 50 100"
min_samples_leaf="5 10 20 50 100"
iterations="100 10 20 50 200"


meses="201404 201403 201402 201401 201312 201311 201310 201309 201308 201307 201306 201305 201304"
source="data_simple_"
output="run_estabilidad_simple.out"

for i in `echo $meses`; do

# cv por mes
result="python run_model2.py --data_file ../data/${source}${i}_I.txt.python  --data_file_size 5 -cv true --iterations 100 --max_depth 20 --min_samples_leaf 20 --min_samples_split 100 --verbose true" 
echo $result
$result

# save model
result="python run_model2.py --data_file ../data/${source}${i}_I.txt.python  --data_file_size 5  --iterations 100 --max_depth 20 --min_samples_leaf 20 --min_samples_split 100 --save_model ../models/${source}${i}_20_20_100_100.model --verbose true"
echo $result
r=`$result`

./pause.sh /tmp/run_estabilidad

for j in `echo $meses`; do
	echo "$i $j"

	./pause.sh /tmp/run_estabilidad

	# load model
	result="python run_model2.py --load_model ../models/${source}${i}_20_20_100_100.model --test_file ../data/${source}${j}_I.txt.python  --test_file_size 5 --verbose true"
	echo $result
	result=`$result`
	echo $result
	gain=`echo $result|grep "Norm test total gain"|tail -n 1|cut -d ':' -f 2`
	echo "$gain $i $j 20 20 10 100" >> $output
done
done

