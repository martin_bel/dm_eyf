s=1
c=gini
d=20
ms=3
ml=20
i=50

for s in {1..10}; do
	file="run_ensamble_${c}_${d}_${ms}_${ml}_${i}_${s}.probs"
	result=`python run_model.py --data_file abril_2014.txt --split_seed $s --criterion $c --max_depth $d --min_samples_split $ms --min_samples_leaf $ml --randomforest true --iterations $i --print_probs probs/$file`
	gain=`echo $result|tail -n 1|cut -d ':' -f 2`
	echo "$gain\t$file" >> run_ensamble.out
done
