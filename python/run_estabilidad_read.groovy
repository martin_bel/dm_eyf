


def input = args[0]
def map = [:]
new File(input).eachLine {
	def tokens = it.trim().split(" ")
	println tokens
	def gain = tokens[0]
	def train = tokens[1]
	def test = tokens[2]

	if ( !map[test] ) map[test] = [:]

	map[test][train] = gain	
}

meses = ["201404", "201403", "201402", "201401", "201312", "201311", "201310", "201309", "201308", "201307", "201306", "201305", "201304"]
print "\t"
meses.each { train -> print train + "\t" }
print "\n"
meses.each { test  ->
	print test + "\t"
	meses.each { train -> 
		print map[test][train] + "\t"
	}
	print "\n"
}
