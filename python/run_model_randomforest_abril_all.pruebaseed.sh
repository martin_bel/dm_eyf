

python run_model2.py --data_file abril_2014.txt --split_seed 100 --criterion entropy --max_depth 20 --min_samples_split 3 --min_samples_leaf 20 --randomforest true --iterations 100
python run_model2.py --data_file abril_2014.txt --split_seed 200 --criterion entropy --max_depth 20 --min_samples_split 3 --min_samples_leaf 20 --randomforest true --iterations 100
python run_model2.py --data_file abril_2014.txt --split_seed 300 --criterion entropy --max_depth 20 --min_samples_split 3 --min_samples_leaf 20 --randomforest true --iterations 100
python run_model2.py --data_file abril_2014.txt --split_seed 400 --criterion entropy --max_depth 20 --min_samples_split 3 --min_samples_leaf 20 --randomforest true --iterations 100
python run_model2.py --data_file abril_2014.txt --split_seed 500 --criterion entropy --max_depth 20 --min_samples_split 3 --min_samples_leaf 20 --randomforest true --iterations 100
python run_model2.py --data_file abril_2014.txt --split_seed 600 --criterion entropy --max_depth 20 --min_samples_split 3 --min_samples_leaf 20 --randomforest true --iterations 100
python run_model2.py --data_file abril_2014.txt --split_seed 700 --criterion entropy --max_depth 20 --min_samples_split 3 --min_samples_leaf 20 --randomforest true --iterations 100
python run_model2.py --data_file abril_2014.txt --split_seed 800 --criterion entropy --max_depth 20 --min_samples_split 3 --min_samples_leaf 20 --randomforest true --iterations 100
python run_model2.py --data_file abril_2014.txt --split_seed 900 --criterion entropy --max_depth 20 --min_samples_split 3 --min_samples_leaf 20 --randomforest true --iterations 100
python run_model2.py --data_file abril_2014.txt --split_seed 1000 --criterion entropy --max_depth 20 --min_samples_split 3 --min_samples_leaf 20 --randomforest true --iterations 100
