setwd("/Users/mbtangotan/Desktop/dm/dm_eyf/abril")

options(scipen=666)
rm(list = ls(all=TRUE))
source('datos_arbol.R')

pkg <- c('caret', 'rpart', 'pROC', 'doMC', 'Hmisc')
sapply(pkg, require, c=T)

# Para procesar en paralelo
registerDoMC(cores = 4)

d <- preprocess(d)

### ADA-BOOST --- CARET ###
predictors <- names(d)[!names(d) %in% "clase"]

set.seed(1)
inTrainingSet <- createDataPartition(
  d$clase,
  p = 0.5,
  list = FALSE
)

train <- d[inTrainingSet, ]
test <- d[-inTrainingSet, ]

ctrl <- trainControl(
  method = "repeatedcv",
  repeats=1,
  classProbs=TRUE
)

Sys.time()
rpartTune <- train(
  x = train[,predictors],
  y = train$clase,
  method = 'rpart',
  metric = 'ROC',
  trControl = ctrl,
  tuneLength=0
)
Sys.time()

# Cambiando tuneLength
# 1 Modelo - 5 min (tuneLength = 0)
# 10 modelos - 6 min
# 100 modelos - 13 min - 1 10 fold CV - cp = 0.00329
# 100 modelos - 10 veces Cross-validation: cp = 0.00538

# ROC was used to select the optimal model using  the largest value.
# The final value used for the model was .cp = 0.00329
# ROC 0.644

### Predicciones
rpartPred <- predict(
  rpartTune,
  test[,predictors]
)
str(rpartPred)

rpartProbs <- predict(
  rpartTune,
  test[,predictors],
  type = "prob"
)
str(rpartProbs)

### Prediccion en base a probabilidades ###

gbmProbs <- predict(gbmTune, test[,predictors], type = "prob")

probs_pred <- ifelse(gbmProbs[,2] > 0.02, 'BAJA', 'CONTINUA')
clase_bin <- ifelse(test$clase == 'BAJA2', 'BAJA', 'CONTINUA')
confusionMatrix(probs_pred, clase_bin)

tbl <- table(probs_pred, clase_bin)
ganancia <- function(tbl){
  tbl[1,1] * 5000 - (sum(tbl[1,]) * 100)
}



### Ganancia ###

ganancia <- function(Probs_BAJA, test_clase){
  library(data.table)
  g = data.table(baja=Probs_BAJA, test_clase)
  gb = g[test_clase == 'BAJA', list(N_BAJA=.N), by=baja]
  gc = g[test_clase == 'CONTINUA', list(N_CONTINUA=.N), by=baja]
  g = merge(gc, gb, by="baja", all=TRUE)
  g[,PROP_B_C:=N_BAJA / (N_BAJA + N_CONTINUA)]
  prop_baja = sum(g$N_BAJA, na.rm=TRUE) / sum(g$N_CONTINUA, na.rm=TRUE)
  g[,ORO:=PROP_B_C / prop_baja]
  g[,GANANCIA:=(N_BAJA * 4900) - ((N_CONTINUA + N_BAJA) * 100)]
  g = g[order(-GANANCIA)]
  g[GANANCIA > 0,sum(GANANCIA)]
  g[,N_BAJA_CUM:=cumsum(N_BAJA)]
  g[,N_CONTINUA_CUM:=cumsum(N_CONTINUA)]
  print(sprintf('GANANCIA: %s', g[GANANCIA > 0, sum(GANANCIA)]))
  g
}

g = ganancia(rpartProbs$BAJA, test$clase)

# Default 872100 - 50% testing
# 1.2 linea de muerte
### split testing=30% training=70%
# Normalizar 1 / 0.3

confusionMatrix(rpartPred, test$clase)

rocCurve <- roc(
  response = test$clase,
  predictor = rpartProbs[, "BAJA"],
  levels = rev(levels(test$clase))
)

plot(rocCurve)

png('roc_rpart_cv.png')
plot(rocCurve)
dev.off()

# Call: roc.default(response = test$clase, predictor = rpartProbs[, "BAJA"],     levels = rev(levels(test$clase)))
# Data: rpartProbs[, "BAJA"] in 147744 controls (test$clase CONTINUA) < 619 cases (test$clase BAJA).
# Area under the curve: 1 CV: 0.7779/ 10 CV: 0.7462
