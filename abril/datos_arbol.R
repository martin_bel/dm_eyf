rm(list = ls(all=TRUE))

library('data.table')
d = fread('abril_20143.txt')

# Gen Features
num <- grep("num_", names(d), v=T)
fl <- grep("fl_", names(d), v=T)
nom <- grep("nom_", names(d), v=T)
fecha <- grep("fecha_", names(d), v=T)
resto <- names(d)[!names(d) %in% c(num, fl, nom, fecha)]

for(col in c(nom, fl)) set(d, j=col, value=as.factor(d[[col]]))
### Clase como factor
d[,clase:=as.factor(clase)]

# id repetido, solo 2048
d[, .N, by=id_numero_de_cliente][N > 1]
d = d[!id_numero_de_cliente %in% 2048]

### Elimino columnas con 1 valor unico, quedan 158 variables
l = unlist(d[,lapply(.SD, function(x) length(unique(x)))])
set(d, j=names(l[l==1]), value = NULL)

### NAs
na = unlist(d[,lapply(.SD, function(x) sum(is.na(x)))])
na_vars = names(na[na > 0])

### Saco id_numero_de_cliente
# d = d[,!names(d) %in% c(fecha, 'id_numero_de_cliente'), with=FALSE]
d = d[,!names(d) %in% c('id_numero_de_cliente'), with=FALSE]

# Convierto a data.frame
d = as.data.frame(d)
d$clase <- as.factor(gsub('\\+', '', d$clase))

source('preprocess.R')
