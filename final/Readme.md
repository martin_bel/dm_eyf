Pasos para obtener el dataset a modelar

## 1) Ejecutar 1_carga_final.R
- Agarra el archivo  producto_premium_2014.txt y le saca columnas con 1 valor y graba dataset.txt

## 2) Ejecutar 2_prep.R
- Agrega como variable id_mes con valores de 0 a 6 siendo 0 el mes de la clase
La idea es que sea facil de cambiar para usar otros meses

- Trata los NAs - Discretiza las numericas con NAs y agrega como nivel NA en las
nominales

- Borro la variable fl_cliente_sucursal. Tiene muchos niveles y no tiene sentido como numerica.

- Me quedo con los id_numero_cliente que estan en el mes 0, el mes a predecir.

- Grabo todo en dataset_modelar.RData para que queden los tipos de datos.
- El codigo de este paso esta comentado en 2_preprocess.R pero por alguna razon no anda.

## 3) features.R

- La idea es generar variables calculadas agregando variables numericas y nominales






