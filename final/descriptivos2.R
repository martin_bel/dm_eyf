# Meses
# train 201204 al 201304 - latencia 201405 - predicción 201406

# d <- d[mes %in% sort(unique(d$mes))[1:25]]
# write.table(d, 'dataset.txt', sep='|', row.names=FALSE)

rm(list = ls(all=TRUE))
setwd('/Users/mbtangotan/Desktop/dm/dm_eyf')

library('RMySQL')
library('data.table')
library('e1071')

d <- fread('dataset.txt')
setkeyv(d, 'id_numero_de_cliente')

# Variables
num <- grep("num_", names(d), v=T)
fl <- grep("fl_", names(d), v=T)
nom <- grep("nom_", names(d), v=T)
fecha <- grep("fecha_", names(d), v=T)
resto <- names(d)[!names(d) %in% c(num, fl, nom, fecha)]

d[,lapply(.SD, function(x){
  quantile(x, probs = 0.25, na.rm=TRUE)
  }), by=clase,.SDcols=num[1:3]]

d[,lapply(.SD, function(x){
  kurtosis(x, na.rm=TRUE)
}),,.SDcols=num]

d[,lapply(.SD, function(x){
  skewness(x, na.rm=TRUE)
}),,.SDcols=num[1:10]]

d[,lapply(.SD, function(x){
  mean(x, na.rm=TRUE)
}),by='mes',.SDcols=num[1:10]]

