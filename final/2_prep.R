# setwd('/Users/mbtangotan/Desktop/dm/dm_eyf/final')
rm(list = ls(all=TRUE))
library('data.table')
library('Hmisc')

table2 <- function(x){
	table(x, useNA='always')
}

d <- fread('dataset.txt')

meses <- c(201309 ,201310, 201311, 201312, 201401, 201402, 201404)
d <- d[mes %in% meses]
mes_lkp <- data.table(mes=sort(unique(d$mes), decreasing=TRUE), id_mes=0:6)
setkey(mes_lkp, mes)
setkey(d, mes)
d <- mes_lkp[d]

num <- grep("num_", names(d), v=T)
fl <- grep("fl_", names(d), v=T)
nom <- grep("nom_", names(d), v=T)
fecha <- grep("fecha_", names(d), v=T)
resto <- names(d)[!names(d) %in% c(num, fl, nom, fecha)]

for(j in c(nom, fl)) {
	set(d, j=j, value=as.factor(d[[j]]))
}

na <- unlist(d[ , lapply(.SD, function(x) sum(is.na(x)))])
na <- names(na[na > 0])

na_num = grep('num_|fecha_', na, value=TRUE)
for (j in na_num){
  set(d, i=NULL, j=j, value = Hmisc::cut2(d[[j]], g = 5))
}

for (j in na_num){
  set(d, i=NULL, j=j, 
  	value = ifelse(is.na(d[[j]]), 'NA', d[[j]]))
}
  
na_nom = grep('nom_|fl_', na, value=TRUE)
for (j in na_nom){
  set(d, i=NULL, j=j, 
  	value = ifelse(is.na(d[[j]]), 'NA', d[[j]]))
}

for(col in c(fl, na_nom, na_num)){
	set(d, j=col, value=as.factor(d[[col]]))
}

set(d, j='clase', value=as.factor(j))
d <- d[fl_participa == 'S']
d[,clase:=as.factor(clase)]

if(sum(is.na(d)) != 0)
    stop('Hay NAs todavía!')

id_0 <- d[id_mes == 0, 'id_numero_de_cliente', with=FALSE]
id_0 <- expand.grid(id_0$id_numero_de_cliente, 0:6)
names(id_0) <- c('id_numero_de_cliente', 'id_mes')
id_0 <- data.table(id_0)
setkey(d, id_mes, id_numero_de_cliente)
setkey(id_0, id_mes, id_numero_de_cliente)
d <- d[id_0, nomatch=0]
set(d, j='fl_cliente_sucursal', value=NULL)

nom2 <- c(fl, na_nom, na_num)

tpdato <- sapply(d, class)
# Numericas sin mes
num <- tpdato[tpdato == 'integer' | tpdato== 'numeric'][-1]
nom <- tpdato[tpdato == 'factor']

save(d, file='dataset_modelar.RData')
