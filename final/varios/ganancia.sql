UPDATE <TABLE>
  SET nod_001 = 20,	pre_001 = 'CONTINUA',	prb_001 = 0.693966
WHERE (Visa_cuenta_estado = 'NA')  AND  (NOT(mcuentas_saldo IS NULL)  AND  (mcuentas_saldo <= -274.58))  AND  (NOT(mdescubierto_preacordado IS NULL)  AND  (mdescubierto_preacordado <= 0));
UPDATE <TABLE>
	SET nod_001 = 21,	pre_001 = 'CONTINUA',	prb_001 = 0.950704
WHERE (Visa_cuenta_estado = 'NA')  AND  (NOT(mcuentas_saldo IS NULL)  AND  (mcuentas_saldo <= -274.58))  AND  ((mdescubierto_preacordado IS NULL) OR (mdescubierto_preacordado > 0));
UPDATE <TABLE>
	SET nod_001 = 22,	pre_001 = 'CONTINUA',	prb_001 = 0.585492
WHERE (Visa_cuenta_estado = 'NA')  AND  (NOT(mcuentas_saldo IS NULL)  AND  (mcuentas_saldo > -274.58  AND  mcuentas_saldo <= 71.33))  AND  (NOT(mcuenta_corriente_Paquete IS NULL)  AND  (mcuenta_corriente_Paquete <= -2679.46));
UPDATE <TABLE>
	SET nod_001 = 23,	pre_001 = 'CONTINUA',	prb_001 = 0.748175
WHERE (Visa_cuenta_estado = 'NA')  AND  (NOT(mcuentas_saldo IS NULL)  AND  (mcuentas_saldo > -274.58  AND  mcuentas_saldo <= 71.33))  AND  (NOT(mcuenta_corriente_Paquete IS NULL)  AND  (mcuenta_corriente_Paquete > -2679.46  AND  mcuenta_corriente_Paquete <= -312.05));
UPDATE <TABLE>
	SET nod_001 = 24,	pre_001 = 'CONTINUA',	prb_001 = 0.921364
WHERE (Visa_cuenta_estado = 'NA')  AND  (NOT(mcuentas_saldo IS NULL)  AND  (mcuentas_saldo > -274.58  AND  mcuentas_saldo <= 71.33))  AND  ((mcuenta_corriente_Paquete IS NULL) OR (mcuenta_corriente_Paquete > -312.05));
UPDATE <TABLE>
	SET nod_001 = 25,	pre_001 = 'CONTINUA',	prb_001 = 0.937500
WHERE (Visa_cuenta_estado = 'NA')  AND  ((mcuentas_saldo IS NULL) OR (mcuentas_saldo > 71.33  AND  mcuentas_saldo <= 16484.91))  AND  (marketing_activo_ultimos90dias = 0);
UPDATE <TABLE>
	SET nod_001 = 26,	pre_001 = 'CONTINUA',	prb_001 = 0.981338
WHERE (Visa_cuenta_estado = 'NA')  AND  ((mcuentas_saldo IS NULL) OR (mcuentas_saldo > 71.33  AND  mcuentas_saldo <= 16484.91))  AND  ((marketing_activo_ultimos90dias IS NULL) OR marketing_activo_ultimos90dias <> 0);
UPDATE <TABLE>
	SET nod_001 = 27,	pre_001 = 'CONTINUA',	prb_001 = 0.995109
WHERE (Visa_cuenta_estado = 'NA')  AND  (NOT(mcuentas_saldo IS NULL)  AND  (mcuentas_saldo > 16484.91))  AND  ((ccheques_emitidos IS NULL) OR (ccheques_emitidos <= 0));
/* Node 28 */.
UPDATE <TABLE>
	SET nod_001 = 28,	pre_001 = 'CONTINUA',	prb_001 = 0.982857
WHERE (Visa_cuenta_estado = 'NA')  AND  (NOT(mcuentas_saldo IS NULL)  AND  (mcuentas_saldo > 16484.91))  AND  (NOT(ccheques_emitidos IS NULL)  AND  (ccheques_emitidos > 0));
/* Node 29 */.
UPDATE <TABLE>
	SET nod_001 = 29,	pre_001 = 'CONTINUA',	prb_001 = 0.976346
WHERE ((Visa_cuenta_estado IS NULL) OR Visa_cuenta_estado <> 'NA'  AND  Visa_cuenta_estado <> '12'  AND  Visa_cuenta_estado <> '19'  AND  Visa_cuenta_estado <> '11')  AND  ((Master_Finiciomora IS NULL) OR Master_Finiciomora <> '20140327'  AND  Master_Finiciomora <> '20140411'  AND  Master_Finiciomora <> '20140314'  AND  Master_Finiciomora <> '20140319'  AND  Master_Finiciomora <> '20140404'  AND  Master_Finiciomora <> '20140306'  AND  Master_Finiciomora <> '20140310'  AND  Master_Finiciomora <> '20140212'  AND  Master_Finiciomora <> '20140414'  AND  Master_Finiciomora <> '20140219'  AND  Master_Finiciomora <> '20140317'  AND  Master_Finiciomora <> '20140331')  AND  (NOT(mpasivos_margen IS NULL)  AND  (mpasivos_margen <= 11.82));
/* Node 30 */.
UPDATE <TABLE>
	SET nod_001 = 30,	pre_001 = 'CONTINUA',	prb_001 = 0.994442
WHERE ((Visa_cuenta_estado IS NULL) OR Visa_cuenta_estado <> 'NA'  AND  Visa_cuenta_estado <> '12'  AND  Visa_cuenta_estado <> '19'  AND  Visa_cuenta_estado <> '11')  AND  ((Master_Finiciomora IS NULL) OR Master_Finiciomora <> '20140327'  AND  Master_Finiciomora <> '20140411'  AND  Master_Finiciomora <> '20140314'  AND  Master_Finiciomora <> '20140319'  AND  Master_Finiciomora <> '20140404'  AND  Master_Finiciomora <> '20140306'  AND  Master_Finiciomora <> '20140310'  AND  Master_Finiciomora <> '20140212'  AND  Master_Finiciomora <> '20140414'  AND  Master_Finiciomora <> '20140219'  AND  Master_Finiciomora <> '20140317'  AND  Master_Finiciomora <> '20140331')  AND  (NOT(mpasivos_margen IS NULL)  AND  (mpasivos_margen > 11.82  AND  mpasivos_margen <= 33.92));
/* Node 31 */.
UPDATE <TABLE>
	SET nod_001 = 31,	pre_001 = 'CONTINUA',	prb_001 = 0.997386
WHERE ((Visa_cuenta_estado IS NULL) OR Visa_cuenta_estado <> 'NA'  AND  Visa_cuenta_estado <> '12'  AND  Visa_cuenta_estado <> '19'  AND  Visa_cuenta_estado <> '11')  AND  ((Master_Finiciomora IS NULL) OR Master_Finiciomora <> '20140327'  AND  Master_Finiciomora <> '20140411'  AND  Master_Finiciomora <> '20140314'  AND  Master_Finiciomora <> '20140319'  AND  Master_Finiciomora <> '20140404'  AND  Master_Finiciomora <> '20140306'  AND  Master_Finiciomora <> '20140310'  AND  Master_Finiciomora <> '20140212'  AND  Master_Finiciomora <> '20140414'  AND  Master_Finiciomora <> '20140219'  AND  Master_Finiciomora <> '20140317'  AND  Master_Finiciomora <> '20140331')  AND  (NOT(mpasivos_margen IS NULL)  AND  (mpasivos_margen > 33.92  AND  mpasivos_margen <= 86.86));
/* Node 32 */.
UPDATE <TABLE>
	SET nod_001 = 32,	pre_001 = 'CONTINUA',	prb_001 = 0.998823
WHERE ((Visa_cuenta_estado IS NULL) OR Visa_cuenta_estado <> 'NA'  AND  Visa_cuenta_estado <> '12'  AND  Visa_cuenta_estado <> '19'  AND  Visa_cuenta_estado <> '11')  AND  ((Master_Finiciomora IS NULL) OR Master_Finiciomora <> '20140327'  AND  Master_Finiciomora <> '20140411'  AND  Master_Finiciomora <> '20140314'  AND  Master_Finiciomora <> '20140319'  AND  Master_Finiciomora <> '20140404'  AND  Master_Finiciomora <> '20140306'  AND  Master_Finiciomora <> '20140310'  AND  Master_Finiciomora <> '20140212'  AND  Master_Finiciomora <> '20140414'  AND  Master_Finiciomora <> '20140219'  AND  Master_Finiciomora <> '20140317'  AND  Master_Finiciomora <> '20140331')  AND  ((mpasivos_margen IS NULL) OR (mpasivos_margen > 86.86));
/* Node 33 */.
UPDATE <TABLE>
	SET nod_001 = 33,	pre_001 = 'CONTINUA',	prb_001 = 0.937500
WHERE ((Visa_cuenta_estado IS NULL) OR Visa_cuenta_estado <> 'NA'  AND  Visa_cuenta_estado <> '12'  AND  Visa_cuenta_estado <> '19'  AND  Visa_cuenta_estado <> '11')  AND  (Master_Finiciomora = '20140327' OR Master_Finiciomora = '20140404' OR Master_Finiciomora = '20140306' OR Master_Finiciomora = '20140212')  AND  ((Visa_marca_atraso IS NULL) OR Visa_marca_atraso <> '1');
/* Node 34 */.
UPDATE <TABLE>
	SET nod_001 = 34,	pre_001 = 'CONTINUA',	prb_001 = 0.869048
WHERE ((Visa_cuenta_estado IS NULL) OR Visa_cuenta_estado <> 'NA'  AND  Visa_cuenta_estado <> '12'  AND  Visa_cuenta_estado <> '19'  AND  Visa_cuenta_estado <> '11')  AND  (Master_Finiciomora = '20140327' OR Master_Finiciomora = '20140404' OR Master_Finiciomora = '20140306' OR Master_Finiciomora = '20140212')  AND  (Visa_marca_atraso = '1');
/* Node 35 */.
UPDATE <TABLE>
	SET nod_001 = 35,	pre_001 = 'CONTINUA',	prb_001 = 0.926174
WHERE ((Visa_cuenta_estado IS NULL) OR Visa_cuenta_estado <> 'NA'  AND  Visa_cuenta_estado <> '12'  AND  Visa_cuenta_estado <> '19'  AND  Visa_cuenta_estado <> '11')  AND  (Master_Finiciomora = '20140411' OR Master_Finiciomora = '20140319' OR Master_Finiciomora = '20140414')  AND  (NOT(ctarjeta_visa_transacciones IS NULL)  AND  (ctarjeta_visa_transacciones <= 3));
/* Node 36 */.
UPDATE <TABLE>
	SET nod_001 = 36,	pre_001 = 'CONTINUA',	prb_001 = 0.970530
WHERE ((Visa_cuenta_estado IS NULL) OR Visa_cuenta_estado <> 'NA'  AND  Visa_cuenta_estado <> '12'  AND  Visa_cuenta_estado <> '19'  AND  Visa_cuenta_estado <> '11')  AND  (Master_Finiciomora = '20140411' OR Master_Finiciomora = '20140319' OR Master_Finiciomora = '20140414')  AND  ((ctarjeta_visa_transacciones IS NULL) OR (ctarjeta_visa_transacciones > 3));
/* Node 37 */.
UPDATE <TABLE>
	SET nod_001 = 37,	pre_001 = 'CONTINUA',	prb_001 = 0.893333
WHERE ((Visa_cuenta_estado IS NULL) OR Visa_cuenta_estado <> 'NA'  AND  Visa_cuenta_estado <> '12'  AND  Visa_cuenta_estado <> '19'  AND  Visa_cuenta_estado <> '11')  AND  (Master_Finiciomora = '20140314' OR Master_Finiciomora = '20140310' OR Master_Finiciomora = '20140219' OR Master_Finiciomora = '20140317' OR Master_Finiciomora = '20140331')  AND  (Visa_marca_atraso = '0');
/* Node 38 */.
UPDATE <TABLE>
	SET nod_001 = 38,	pre_001 = 'CONTINUA',	prb_001 = 0.701923
WHERE ((Visa_cuenta_estado IS NULL) OR Visa_cuenta_estado <> 'NA'  AND  Visa_cuenta_estado <> '12'  AND  Visa_cuenta_estado <> '19'  AND  Visa_cuenta_estado <> '11')  AND  (Master_Finiciomora = '20140314' OR Master_Finiciomora = '20140310' OR Master_Finiciomora = '20140219' OR Master_Finiciomora = '20140317' OR Master_Finiciomora = '20140331')  AND  ((Visa_marca_atraso IS NULL) OR Visa_marca_atraso <> '0');
/* Node 39 */.
UPDATE <TABLE>
	SET nod_001 = 39,	pre_001 = 'BAJA+1',	prb_001 = 0.362963
WHERE (Visa_cuenta_estado = '12')  AND  ((mdescubierto_preacordado IS NULL) OR (mdescubierto_preacordado <= 0))  AND  ((mcaja_ahorro_Paquete IS NULL) OR (mcaja_ahorro_Paquete <= 0));
/* Node 40 */.
UPDATE <TABLE>
	SET nod_001 = 40,	pre_001 = 'CONTINUA',	prb_001 = 0.666667
WHERE (Visa_cuenta_estado = '12')  AND  ((mdescubierto_preacordado IS NULL) OR (mdescubierto_preacordado <= 0))  AND  (NOT(mcaja_ahorro_Paquete IS NULL)  AND  (mcaja_ahorro_Paquete > 0));
/* Node 41 */.
UPDATE <TABLE>
	SET nod_001 = 41,	pre_001 = 'CONTINUA',	prb_001 = 0.933054
WHERE (Visa_cuenta_estado = '12')  AND  (NOT(mdescubierto_preacordado IS NULL)  AND  (mdescubierto_preacordado > 0))  AND  ((Master_marca_atraso IS NULL) OR Master_marca_atraso <> '1');
/* Node 42 */.
UPDATE <TABLE>
	SET nod_001 = 42,	pre_001 = 'CONTINUA',	prb_001 = 0.771930
WHERE (Visa_cuenta_estado = '12')  AND  (NOT(mdescubierto_preacordado IS NULL)  AND  (mdescubierto_preacordado > 0))  AND  (Master_marca_atraso = '1');
/* Node 43 */.
UPDATE <TABLE>
	SET nod_001 = 43,	pre_001 = 'CONTINUA',	prb_001 = 0.546875
WHERE (Visa_cuenta_estado = '19')  AND  ((ttarjeta_master IS NULL) OR ttarjeta_master <> 'S')  AND  (NOT(mdescubierto_preacordado IS NULL)  AND  (mdescubierto_preacordado <= 0));
/* Node 44 */.
UPDATE <TABLE>
	SET nod_001 = 44,	pre_001 = 'CONTINUA',	prb_001 = 0.470199
WHERE (Visa_cuenta_estado = '19')  AND  ((ttarjeta_master IS NULL) OR ttarjeta_master <> 'S')  AND  ((mdescubierto_preacordado IS NULL) OR (mdescubierto_preacordado > 0));
/* Node 17 */.
UPDATE <TABLE>
	SET nod_001 = 17,	pre_001 = 'CONTINUA',	prb_001 = 0.930000
WHERE (Visa_cuenta_estado = '19')  AND  (ttarjeta_master = 'S');
/* Node 45 */.
UPDATE <TABLE>
	SET nod_001 = 45,	pre_001 = 'BAJA+1',	prb_001 = 0.693548
WHERE (Visa_cuenta_estado = '11')  AND  (NOT(cprestamos_personales IS NULL)  AND  (cprestamos_personales <= 0))  AND  (NOT(cliente_antiguedad IS NULL)  AND  (cliente_antiguedad <= 75));
/* Node 46 */.
UPDATE <TABLE>
	SET nod_001 = 46,	pre_001 = 'BAJA+1',	prb_001 = 0.450704
WHERE (Visa_cuenta_estado = '11')  AND  (NOT(cprestamos_personales IS NULL)  AND  (cprestamos_personales <= 0))  AND  ((cliente_antiguedad IS NULL) OR (cliente_antiguedad > 75));
/* Node 47 */.
UPDATE <TABLE>
	SET nod_001 = 47,	pre_001 = 'CONTINUA',	prb_001 = 0.424460
WHERE (Visa_cuenta_estado = '11')  AND  ((cprestamos_personales IS NULL) OR (cprestamos_personales > 0))  AND  ((mcaja_ahorro_Paquete IS NULL) OR (mcaja_ahorro_Paquete <= 0));
/* Node 48 */.
UPDATE <TABLE>
	SET nod_001 = 48,	pre_001 = 'CONTINUA',	prb_001 = 0.750000
WHERE (Visa_cuenta_estado = '11')  AND  ((cprestamos_personales IS NULL) OR (cprestamos_personales > 0))  AND  (NOT(mcaja_ahorro_Paquete IS NULL)  AND  (mcaja_ahorro_Paquete > 0));
