# setwd("/Users/mbtangotan/Desktop/dm/dm_eyf/final/agg/cachos")
options(scipen=666)
rm(list = ls(all=TRUE))

pkg = c('foreign', 'nnet', 'ggplot2', 'caret', 'glmnet', 
	    'doParallel', 'doMC', 'svmpath', 'LiblineaR',
	    'data.table')
sapply(pkg, require, c=T)

prep_td <- function(dataset){
	d <- fread(dataset)
	set(d, j='bins', value=NULL)
	num <- grep("num_", names(d), v=T)
	fc_num <- grep("fc_", names(d), v=T)
	fl <- grep("fl_", names(d), v=T)
	nom <- grep("nom_", names(d), v=T)
	fc_nom <- grep("fc_nom", names(d), v=T)
	resto <- names(d)[!names(d) %in% c(num, fl, nom, fc_nom, fc_num)]

	for(j in c(nom, fc_nom, 'clase')) {
		set(d, j=j, value=as.factor(d[[j]]))
	}

	for(j in c(num, fc_num, nom)) {
		set(d, j=j, value=as.numeric(d[[j]]))
	}


	td <- data.frame(dato=sapply(d, class))
	td$nm <- row.names(td)
	char <- td[td[,1] == 'character', 'nm']

	for(j in char) {
		set(d, j=j, value=as.factor(d[[j]]))
	}

	na <- unlist(d[, lapply(.SD, function(x) sum(is.na(x)))])
	for(j in names(na[na != 0])){
		set(d, i=NULL, j=j, 
			value=ifelse(
				is.numeric(d[[j]]), mean(d[[j]], na.rm=T), 'NA'
				)
			)
	}

	ln <- unlist(d[, lapply(.SD, function(x) length(unique(x))) ])
	drop <- names(ln[ln == 1])
	d <- d[, names(d)[!names(d) %in% drop] , with=F]

	return(d)
}

