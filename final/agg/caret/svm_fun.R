### SVM funciones ### 

fit1 <- function(heurC, verbose=TRUE, model_type=0,
	wi_default=TRUE, wi = table(y) / length(y),
	x_train=x_train, ...){
	if(wi_default){
		svm = LiblineaR(data = x_train, labels = y, type=model_type,
	        cost = heurC, bias=TRUE, verbose=verbose)
	} else {
		names(wi) = as.character(levels(y))
	    svm = LiblineaR(data = x_train, labels = y, type=model_type, wi=wi,
	        cost = heurC, bias=TRUE, verbose=verbose)
	}
    return(svm)
}

train_lr <- function(heurC, model_type=1, wi=c(115, 1), ...){
	svm <- fit1(heurC, verbose=TRUE, model_type=model_type, 
		x_train=x_train, wi_default=FALSE, wi=wi, ...)
	pred <- predict(svm, x_test, proba=FALSE)[[1]]
	tbl <- table(pred, y_test)
	gan <- ganancia(tbl)
	list(model=svm, tbl=tbl, gan=gan)
} 

cut_off <- function(svm, cut, prob_bool=TRUE, x_test=x_test){
	if(prob_bool){
		probs <- predict(svm, x_test, proba=prob_bool)$probabilities
		pred <- ifelse(probs[,2] > cut, 'BAJA', 'CONTINUA')
	} else {
		pred <- predict(svm, x_test, proba=prob_bool)[[1]]
	}
	tbl = table(pred, y_test)
	gan = ganancia(tbl)
	print(sprintf('Ganancia: %s Cut off: %s', gan, cut))
}

ganancia <- function(tbl){
  tbl[1,1] * 5000 - (sum(tbl[1,]) * 100)
}
