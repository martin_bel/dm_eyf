preprocess <- function(d){
  d$clase <- as.factor(d$clase)
  
  na = apply(d, 2, function(x) sum(is.na(x)))
  na = na[na != 0]
  
  ## NAs numéricas - Discretizo y reemplazo por categoria NA
  na_num = grep('num_|fecha_',names(na), value=TRUE)
  d[,na_num] <- apply(d[,na_num], 2, function(x) Hmisc::cut2(x, g=5))
  d[,na_num] <- apply(d[,na_num], 2, function(x) ifelse(is.na(x), "NA", x))
  
  ## NAs nominales, reemplazo por la categoria "NA"
  na_nom = grep('nom_|fl_',names(na), value=TRUE)
  d[,na_nom] <- apply(d[,na_nom], 2, function(x) ifelse(is.na(x), "NA", x))
  
  for(j in c(na_nom, na_num)) d[,j] <- as.factor(d[,j])
  
  if(sum(is.na(d)) != 0)
    stop('Hay NAs todavía!')
  return(d)
}

ganancia <- function(tbl){
  tbl[1,1] * 5000 - (sum(tbl[1,]) * 100)
}

d$clase <- as.factor(d$clase)